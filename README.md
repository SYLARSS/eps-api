# Installation Guide

## 1. Running VirutalEnv over Windows machine

As first step we have to go to the project's root folder. 
```
cd path/to/pybase
```

After that, we must be sure virtualenv has been installed already. With that command we are going to install the package, globally. 
```
pip install virtualenv
```

After we get installed virtualenv, we need to add new venv in the root folder of the project
```
virtualenv venv
```
Note: This might take a time, keep calm and wait for it.

After we have prepared a virtal environment, we need to install all requirements the project has

```
cd path/to/pybase
\path\to\venv\Scripts\activate
```
## Example:
```
C:\Users\'Username'\PycharmProjects\pybase\venv\Scripts\activate
```

After we are ready with this, we can install all requirements
```
pip install -r application/requirements.txt
```



Before we hit the project on running, we need to prepare some enviornments which are important for flask module

```
export FLASK_APP=app
export FLASK_ENV=development|production|stage|testing
export FLASK_CONFIG=development|production|stage|testing
```
Keep in mind, the lifecycle of those variables getting finished once you close the terminal. 

And finally we could run the application
```
flask run --host=0.0.0.0
```



# Troubleshooting 
Keep in mind some libraries might throw an exception if you have not installed Visual C++ ( To be sure, please use the latest version )
```
https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads
```

Some of libraries might require win32api module, you can install it with: 
```
pip install pypiwin32
```

