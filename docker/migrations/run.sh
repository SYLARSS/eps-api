#!/bin/bash
set -e
cd /usr/src/app && flask db migrate && flask db upgrade && python manage.py create_volume_partitions
