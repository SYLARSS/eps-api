GIT_COMMIT = $(shell git rev-parse --verify HEAD)
DOCKER_REPO = registry.gitlab.xatonax.com
DOCKER_IMAGE_NAME = eps/eps
DOCKER_HOST = registry.xatonax.com
APPLICATION_NAME = api
## Name of the container that you will SSH
workspace=eps_api
## EXAMPLE: env=[dev, local, prod, stage, testing]
env=local
## Name of the main Dockerfile
dockerfile=Dockerfile
COMPOSE=docker-compose -f docker-compose.yml -f
local=docker-compose-local.yml
prod=docker-compose-prod.yml

## Put it first so that "make" without argument is like "make help".
##@ Helpers
help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


##@ Docker Compose Commands

compose-build: ## Build Docker Compose
	$(COMPOSE) $(${env}) build

compose-build-fresh: ## Build without cache .Dockerfile
	$(COMPOSE) $(${env}) build --no-cache

compose-run: ## Run Docker Compose
	$(COMPOSE) $(${env}) up --force-recreate

compose-down: ## Stop Docker Compose
	$(COMPOSE) $(${env}) down --remove-orphans

compose-fresh: ## Full restart on the composer stack services
	$(MAKE) compose-down
	$(MAKE) clean
	$(MAKE) compose-build-fresh
	$(MAKE) compose-run

compose-config: ## Print the .yml config file of choice
	$(COMPOSE) $(${env}) config

##@ Docker Commands

# -----------------------------------------------------------------------------
#build an image and append proper tags
# -----------------------------------------------------------------------------
build: ## Build Docker
	docker build -t $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):$(GIT_COMMIT) -t  $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):latest -f $(dockerfile) .

# -----------------------------------------------------------------------------
# Build an image without cache
# -----------------------------------------------------------------------------
build-fresh: ## Build Docker no cache
	docker build --no-cache -t $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):$(GIT_COMMIT) -t  $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):latest -f $(dockerfile) .

# -----------------------------------------------------------------------------
#get the docker login
# -----------------------------------------------------------------------------
login: ## Login to repo
	docker login $(DOCKER_REPO)

# -----------------------------------------------------------------------------
#push to repo
# -----------------------------------------------------------------------------
push: ## Push Docker image to repo
	docker push $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):$(GIT_COMMIT)
	docker push $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):latest

# -----------------------------------------------------------------------------
# Run the docker Image
# -----------------------------------------------------------------------------
run: ## Run Docker
	docker run  -d --name xatonax_$(APPLICATION_NAME) -d  $(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)/$(APPLICATION_NAME):latest

##@ API Commands
api_ssh: ## Connect to API container
	docker exec -it $(workspace) /bin/bash

clean: ## Delete all containers, images and volumes
	docker system prune -a -f
	docker volume prune -f
