#!/usr/bin/env python
# http://www.rabbitmq.com/tutorials/tutorial-two-python.html
import pika
import os
from common.rabbitmq import RabbitMQ

credentials = pika.PlainCredentials(os.environ.get('RABBIT_USER'), os.environ.get('RABBIT_PASS'))
parameters = pika.ConnectionParameters(os.environ.get('RABBIT_HOST'), os.environ.get('RABBIT_PORT'), '/',
                                       credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()


def callback(ch, method, properties, body):
    print("Processing message: {}".format(body))
    # replace with condition for rejection
    if True:
        print("Rejecting message")
        ch.basic_nack(method.delivery_tag, False, False)


producer = RabbitMQ()

producer.consume(queue=os.environ.get('RABBIT_FUTURE_SOURCE_MAIN_QUEUE'), function=callback)
