from schemas import ExchangeVolumeSchema
from app import db
from flask_rest_jsonapi import ResourceDetail
from app.models import ExchangeVolumeModel
from helpers.auth_helper import requires_auth


class ExchangeVolumeDetail(ResourceDetail):
    """
    Exchange Volume Details
    """

    decorators = (requires_auth,)
    schema = ExchangeVolumeSchema
    data_layer = {
        'session': db.session,
        'model': ExchangeVolumeModel,
    }
