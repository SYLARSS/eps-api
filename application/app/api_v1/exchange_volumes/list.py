from schemas import ExchangeVolumeSchema
from app import db
from app.models import ExchangeVolumeModel, ExchangeModel
from helpers.auth_helper import requires_auth
from flask_rest_jsonapi import ResourceList
from common.decorators import query_list_volumes


class ExchangeVolumeList(ResourceList):
    """
    Exchange Volume List

    """

    def create_object(self, data, kwargs):
        """

        :param data:
        :param kwargs:
        :return:
        """
        if kwargs.get('exchange_id'):
            exchange_volume_model = self._data_layer.session.query(ExchangeVolumeModel) \
                .filter_by(exchange_id=kwargs.get('exchange_id'), date=data.get('date')) \
                .first()
            if exchange_volume_model is not None:
                for key, item in data.items():
                    setattr(exchange_volume_model, key, item)
                self._data_layer.session.commit()
                return exchange_volume_model

        return super().create_object(data, kwargs)

    decorator_values = {
        "exchange_id": {
            "primary_model": ExchangeVolumeModel,
            "secondary_model": ExchangeModel,
            "primary_model_relation": "exchanges",
            "filter_secondary_model_key": "id",
        }
    }

    @query_list_volumes(decorator_values)
    def query(self, view_kwargs):
        """
        :param view_kwargs:
        :return:
        """

    def before_create_object(self, data, view_kwargs):
        """
        Before create object append exchange_id to request
        :param data: 
        :param view_kwargs: 
        :return: 
        """""
        if view_kwargs.get('exchange_id') is not None:
            exchange = self.session.query(ExchangeModel).filter_by(id=view_kwargs['exchange_id']).one()
            data['exchange_id'] = exchange.id

    decorators = (requires_auth,)
    view_kwargs = True
    view_args = True
    schema = ExchangeVolumeSchema
    methods = ['GET', 'POST']
    data_layer = {
        'session': db.session,
        'model': ExchangeVolumeModel,
        'methods': {
            'query': query,
            'before_create_object': before_create_object,
        }
    }
