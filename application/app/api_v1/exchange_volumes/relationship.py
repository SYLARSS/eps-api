from flask_rest_jsonapi import ResourceRelationship
from app import db
from app.models import ExchangeVolumeModel
from schemas import ExchangeVolumeSchema
from helpers.auth_helper import requires_auth


class ExchangeVolumeRelationship(ResourceRelationship):
    """
    Exchange Volume relationship
    """
    decorators = (requires_auth,)
    schema = ExchangeVolumeSchema
    data_layer = {
        'session': db.session,
        'model': ExchangeVolumeModel
    }
