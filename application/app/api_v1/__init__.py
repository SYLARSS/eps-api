from app.bootstrap import api

from app.api_v1.users import *
from app.api_v1.exchanges import *
from app.api_v1.exchange_volumes import *

# User
api.route(UserList, 'user_list',
          '/users',
          '/exchanges/<int:exchange_id>/users'
          )
api.route(UserDetail, 'user_detail',
          '/users/<int:id>',
          )
api.route(UserRelationship, 'user_exchanges',
          '/users/<int:id>/relationships/exchanges',
          )

# Exchange
api.route(ExchangeList, 'exchange_list',
          '/exchanges',
          '/users/<int:user_id>/exchanges',
          )
api.route(ExchangeDetail, 'exchange_detail',
          '/exchanges/<int:id>',
          '/volumes/<int:volume_id>/exchange'
          )
api.route(ExchangeRelationship, 'exchange_users', '/exchanges/<int:id>/relationships/users', )
api.route(ExchangeRelationship, 'exchange_volumes', '/exchanges/<int:id>/relationships/volumes', )

# Exchange Source Volumes
api.route(ExchangeVolumeList, 'exchange_volume_list',
          '/volumes',
          '/exchanges/<int:exchange_id>/volumes'
          )
api.route(ExchangeVolumeDetail, 'exchange_volume_detail',
          '/volumes/<int:id>',
          )
api.route(ExchangeRelationship, 'volume_exchange',
          '/volumes/<int:id>/relationships/exchange'
          )
