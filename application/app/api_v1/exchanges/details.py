from schemas import ExchangeSchema
from app import db
from flask_rest_jsonapi import ResourceDetail
from app.models import ExchangeModel
from helpers.auth_helper import requires_auth
from common.decorators import check_current_user_has_relation


class ExchangeDetail(ResourceDetail):
    """
    Exchange detail by id
    """

    @check_current_user_has_relation(param='id', relation='exchanges', relation_param='id')
    def before_get_object(self, view_kwargs):
        """
        before get object method for exchange detail
        :param view_kwargs:
        :return:
        """

    decorators = (requires_auth,)
    schema = ExchangeSchema
    data_layer = {
        'session': db.session,
        'model': ExchangeModel,
        'methods': {
            'before_get_object': before_get_object,
        }
    }
