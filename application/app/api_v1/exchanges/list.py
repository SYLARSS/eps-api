from schemas import ExchangeSchema
from app import db
from app.models import ExchangeModel
from helpers.auth_helper import requires_auth
from flask_rest_jsonapi import ResourceList
from app.models import UserModel
from common.source_producer import SourceProducer
from common.decorators import query_decorator_has_exchange


class ExchangeList(ResourceList):
    """
    List and create Exchange
    """

    def after_post(self, result):
        """
        After create Exchange
        :param result:
        :return:
        """
        data = result[0]['data']
        attributes = data['attributes']
        future_source = attributes['future-source']
        history_source = attributes['history-source']
        SourceProducer(future_source, history_source, data['id'])
        return result

    @query_decorator_has_exchange(
        first_model=ExchangeModel,
        second_model=UserModel,
        view_kwargs_param='user_id',
        filter_second_model_key='id',
        first_model_relation='exchanges',
        relation_param='id'
    )
    def query(self, view_kwargs):
        """
        :param view_kwargs:
        :return:
        """

    decorators = (requires_auth,)
    view_kwargs = True
    schema = ExchangeSchema
    methods = ['GET', 'POST']
    data_layer = {
        'session': db.session,
        'model': ExchangeModel,
        'methods': {
            'query': query,
        }
    }
