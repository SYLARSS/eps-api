from flask_rest_jsonapi import ResourceRelationship
from app import db
from app.models import ExchangeModel
from schemas import ExchangeSchema
from helpers.auth_helper import requires_auth


class ExchangeRelationship(ResourceRelationship):
    """
    Exchange Relationship
    """
    decorators = (requires_auth,)
    schema = ExchangeSchema
    data_layer = {
        'session': db.session,
        'model': ExchangeModel
    }
