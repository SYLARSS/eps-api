from flask_rest_jsonapi import ResourceRelationship
from app import db
from app.models import UserModel
from schemas import UserSchema
from helpers.auth_helper import requires_auth


class UserRelationship(ResourceRelationship):
    """
    User Relationship
    """
    decorators = (requires_auth,)
    schema = UserSchema
    data_layer = {
        'session': db.session,
        'model': UserModel
    }
