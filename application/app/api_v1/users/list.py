from schemas import UserSchema
from app import db
from app.models import UserModel
from helpers.auth_helper import requires_auth
from flask_rest_jsonapi import ResourceList
from helpers.text_helper import generate_key
from app.models import ExchangeModel
from common.decorators import query_decorator_simple


class UserList(ResourceList):
    """
    List and create Users
    """
    decorator_values = {
        "exchange_id": {
            "primary_model": UserModel,
            "secondary_model": ExchangeModel,
            "primary_model_relation": "exchanges",
            "filter_secondary_model_key": "id",
        }
    }

    @query_decorator_simple(decorator_values)
    def query(self, view_kwargs):
        """
        query method for class
        :param view_kwargs:
        :return:
        """

    def before_create_object(self, data, view_kwargs):
        data['api_key'] = str(generate_key(32))

    decorators = (requires_auth,)
    schema = UserSchema
    view_kwargs = True
    methods = ['GET', 'POST']
    data_layer = {
        'session': db.session,
        'model': UserModel,
        'methods': {
            'query': query,
            'before_create_object': before_create_object,
        }
    }
