from schemas import UserSchema
from app import db
from flask_rest_jsonapi import ResourceDetail
from app.models import UserModel
from helpers.auth_helper import requires_auth


class UserDetail(ResourceDetail):
    """
    User detail by id
    """

    decorators = (requires_auth,)
    schema = UserSchema
    data_layer = {
        'session': db.session,
        'model': UserModel,
    }
