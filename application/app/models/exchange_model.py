from sqlalchemy.sql import func
from app import db

MAX_STRING_LENGTH = 255
MAX_NAME_LENGTH = 32


class ExchangeModel(db.Model):
    """
    Exchange Model
    """
    __tablename__ = 'exchanges'
    _s_type = 'exchange'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(MAX_NAME_LENGTH), unique=True, nullable=False)
    website = db.Column(db.String(MAX_STRING_LENGTH), nullable=False)
    future_source = db.Column(db.String(MAX_STRING_LENGTH), nullable=False, comment='URL')
    history_source = db.Column(db.String(MAX_STRING_LENGTH), nullable=False, comment='URL')
    created_at = db.Column(db.DateTime(timezone=True), default=func.now())

    users = db.relationship("UserModel", secondary='user_exchanges', back_populates="exchanges")
    volumes = db.relationship("ExchangeVolumeModel", back_populates="exchange")
