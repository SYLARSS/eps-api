from sqlalchemy.sql import func
from app import db


class ExchangeVolumeModel(db.Model):
    """
    Exchange Volume Model
    """
    __tablename__ = 'exchange_volumes'

    __table_args__ = (
        db.UniqueConstraint('exchange_id', 'date'),
        {'postgresql_partition_by': 'RANGE (date)'},
    )
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    exchange_id = db.Column(db.Integer, db.ForeignKey('exchanges.id', ondelete='CASCADE'), nullable=False)
    date = db.Column(db.DateTime, nullable=False, primary_key=True)
    volume_24_usd = db.Column(db.BigInteger, nullable=True)
    volume_24_btc = db.Column(db.BigInteger, nullable=True)
    volume_24_eth = db.Column(db.BigInteger, nullable=True)
    number_of_pairs = db.Column(db.BigInteger, nullable=True)
    number_of_coins = db.Column(db.BigInteger, nullable=True)
    created_at = db.Column(db.DateTime(timezone=True), default=func.now())

    exchange = db.relationship("ExchangeModel", back_populates="volumes")
