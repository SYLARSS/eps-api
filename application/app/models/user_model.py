from sqlalchemy.sql import func
from app import db
from sqlalchemy.orm import validates
from helpers.text_helper import create_assert_message
import re

MAX_FULL_NAME_LENGTH = 120
MAX_API_KEY_LENGTH = 32
MAX_EMAIL_LENGTH = 255


class UserModel(db.Model):
    """
    User Model
    """
    __tablename__ = 'users'
    _s_type = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # some contact email
    email = db.Column(db.String(MAX_EMAIL_LENGTH), unique=True, nullable=False)
    # some name of organization use this credentials
    full_name = db.Column(db.String(MAX_FULL_NAME_LENGTH), nullable=False)
    # API Key generated for this organization
    api_key = db.Column(db.String(MAX_API_KEY_LENGTH), unique=True, nullable=False)
    # is active the account or not
    active = db.Column(db.Boolean, nullable=False, default=False)
    # when the organization was made
    created_at = db.Column(db.DateTime(timezone=True), default=func.now())

    exchanges = db.relationship("ExchangeModel", secondary='user_exchanges', back_populates="users")

    @validates('full_name')
    def validate_name(self, key, name):
        assert type(name) is str, create_assert_message(key, "The 'Name' attribute must be a string")
        assert len(name) <= MAX_FULL_NAME_LENGTH, create_assert_message(
            key,
            "The 'Name' length must be under {} symbols".format(MAX_FULL_NAME_LENGTH)
        )
        return name

    @validates('email')
    def validate_email(self, key, value):
        assert type(value) is str, create_assert_message(key, "The 'Email' attribute must be a string")
        assert len(value) <= MAX_EMAIL_LENGTH, create_assert_message(
            key,
            "The 'Email' length must be under {} symbols".format(MAX_EMAIL_LENGTH)
        )
        assert not re.match(r'/^\S+@\S+$/', value), create_assert_message(
            key,
            "The provided email value is invalid.".format(MAX_EMAIL_LENGTH)
        )

        return value

    @validates('api_key')
    def validate_api_key(self, key, api_key):
        assert type(api_key) is str, create_assert_message(key, "Api key must be a string")
        assert len(api_key) == MAX_API_KEY_LENGTH, create_assert_message(
            key,
            "The API Key must be {} symbols".format(MAX_API_KEY_LENGTH)
        )
        return api_key

    @validates('active')
    def validate_active(self, key, active):
        assert type(active) is bool, create_assert_message(key, "Active attribute must be a boolean")
        return active
