from sqlalchemy.sql import func
from app import db


class UserExchangeModel(db.Model):
    """
    User Exchange Model
    """
    __tablename__ = 'user_exchanges'
    __table_args__ = (
        db.UniqueConstraint('user_id', 'exchange_id'), {}
    )
    exchange_id = db.Column(db.Integer, db.ForeignKey('exchanges.id', ondelete='CASCADE'), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete='CASCADE'), primary_key=True)
    created_at = db.Column(db.DateTime(timezone=True), default=func.now())
