from flask import Blueprint, current_app as app
from flask_rest_jsonapi import Api
from helpers.auth_helper import permission_manager


api_v1 = Blueprint('v1', __name__, url_prefix='/v1')
api = Api(app=app, blueprint=api_v1)
api.permission_manager(permission_manager)
