import factory
from app.models.user_model import UserModel, MAX_API_KEY_LENGTH
from app import db


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    User Factory
    """
    class Meta:
        model = UserModel
        sqlalchemy_session = db.session

    email = factory.Faker('email')
    full_name = factory.Faker('name')
    api_key = factory.Faker('pystr', min_chars=MAX_API_KEY_LENGTH, max_chars=MAX_API_KEY_LENGTH)
    active = True
    id = None
    created_at = None
