import factory
from app.models.exchange_model import ExchangeModel
from app import db


class ExchangeFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = ExchangeModel
        sqlalchemy_session = db.session

    name = factory.Faker('name')
    website = factory.Faker('url')
    history_source = factory.Faker('url')
    future_source = factory.Faker('url')
    id = None
    created_at = None
