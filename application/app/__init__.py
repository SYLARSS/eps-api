from flask import Flask, send_from_directory

from flask_migrate import Migrate
from config import app_config
import os
from flask_rest_jsonapi import JsonApiException
from flask import make_response, json
from flask_rest_jsonapi.errors import jsonapi_errors
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_continuum import make_versioned
from sqlalchemy_continuum.plugins import FlaskPlugin
from flask_swagger_ui import get_swaggerui_blueprint

make_versioned(plugins=[FlaskPlugin()], options={
    'strategy': 'subquery'
})

db = SQLAlchemy()
SWAGGER_URL = '/swagger'  # URL for exposing Swagger UI (without trailing '/')
API_URL = '/static/swagger.json'  # Our API url (can of course be a local resource)
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "EPS Swagger"
    },
)


def create_app():
    _app = Flask(__name__, instance_relative_config=True)
    _app.config.from_object(app_config[os.getenv('FLASK_CONFIG', 'production')])
    Migrate(_app, db)
    db.init_app(_app)

    # @todo add Flask_script Manager to exec Commands
    # @todo add Implement some config cache behaviours
    # @todo implement Cors for MS
    # CORS(app, resources={r"/*": {"origins": "*"}})
    @_app.errorhandler(AssertionError)
    def assertion_server_error(error):
        error = error.args[0]
        exc = JsonApiException(
            detail=error['error'],
            title=error['title'],
            status='422',
            source='Invalid parameter'
        )
        return make_response(
            json.dumps(jsonapi_errors([exc.to_dict()])),
            exc.status,
            {'Content-Type': 'application/vnd.api+json'},
        )

    with _app.app_context():
        import app.api_v1
        from app import models
        from app.bootstrap import api_v1
        _app.register_blueprint(api_v1)
        _app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

    @_app.route('/static/<path:path>')
    def send_static(path):
        return send_from_directory('static', path)

    return _app, db


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
pyBase, database = create_app()

if __name__ == '__main__':
    pyBase.run(debug=True)
