from helpers.source_helper import getSource
from app.models import ExchangeModel
from common.rabbitmq import RabbitMQ
import requests
import json
from lxml import html


class SourceProducer:
    """
    Source Producer:
    Create rabbit messages with the source urls to be scraped
    """

    def __init__(self, future_url, history_url, exchange_id):
        source = getSource(history_url)
        f_source = getSource(future_url)
        self.future_url = future_url
        self.future_source = f_source['source']
        self.future_slug = f_source['slug']
        self.history_url = history_url
        self.exchange = ExchangeModel.query.filter_by(id=exchange_id).first()
        self.history_source = source['source']
        self.history_slug = source['slug']
        self.publishSources()

    def publishSources(self):
        """
        Execute function by source
        """
        future_sources_available = ['coinpaprika', 'coinmarketcap', 'coingecko']
        history_sources_available = ['cryptocompare', 'coingecko']

        if self.future_source not in future_sources_available:
            print('Future Source not found. Available are [coinmarketcap, coinpaprika, coingecko]')
            return

        if self.history_source not in history_sources_available:
            print('History Source not found. Available are [cryptocompare, coingecko]')
            return

        history_class_ = getattr(self.__class__, self.history_source + '_history_urls')
        future_class_ = getattr(self.__class__, self.future_source + '_url')
        history_class_(self)
        future_class_(self)

    def cryptocompare_history_urls(self):
        """
        Crypto Compare History URL
        """
        rabbit_data = {
            'exchange_id': self.exchange.id,
            'source_url': self.history_url,
            'source_type': self.history_source
        }

        self.publishToRabbit(
            routing_key='RABBIT_EXCHANGE_HISTORY_SOURCE_QUEUE',
            queue='RABBIT_EXCHANGE_HISTORY_SOURCE_QUEUE',
            exchange='',
            data=rabbit_data
        )

    def cryptocompare_url(self):
        """
        Crypto Compare Future Source URL
        Usage with DLX messages for rabbit mq
        """
        rabbit_data = {
            'exchange_id': self.exchange.id,
            'source_url': self.future_url,
            'source_type': self.future_source
        }

        self.publishToRabbit(
            exchange='RABBIT_FUTURE_SOURCE_DLX_EXCHANGE',
            routing_key='RABBIT_FUTURE_SOURCE_MAIN_QUEUE',
            dlx=True,
            data=rabbit_data
        )

    def coinpaprika_url(self):
        """
        Coinpaprika future url
        usage with DLX messages
        """
        rabbit_data = {
            'exchange_id': self.exchange.id,
            'source_url': self.future_url,
            'source_type': self.future_source
        }

        self.publishToRabbit(
            exchange='RABBIT_FUTURE_SOURCE_DLX_EXCHANGE',
            routing_key='RABBIT_FUTURE_SOURCE_MAIN_QUEUE',
            dlx=True,
            data=rabbit_data
        )

    def coingecko_history_urls(self):
        """
        Coin Gecko Future Source URL
        Generates url to be scraped and publish them to rabbit mq
        """
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Origin": "*",
            'cache-control': "max-age=0",
            'content-type': "application/json",
        }
        req = requests.Session()
        cg_id = None
        response = req.request("GET", self.history_url, headers=headers)
        tree = html.fromstring(response.text)
        try:
            link_with_id = tree.xpath('//*[@id="statistics"]/div[2]/div[2]/div/div/div/a[1]/@data-url')
            cg_id = link_with_id[0].split('/')[2]
        except Exception:
            print('Invalid HTML or no Coingecko statistics.')
        rabbit_data = {
            'exchange_id': self.exchange.id,
            'source_url': self.history_url,
            'source_type': self.history_source,
            'source_id': cg_id
        }

        self.publishToRabbit(
            routing_key='RABBIT_EXCHANGE_HISTORY_SOURCE_QUEUE',
            queue='RABBIT_EXCHANGE_HISTORY_SOURCE_QUEUE',
            exchange='',
            data=rabbit_data
        )

    def coingecko_url(self):
        """
        Coingecko future url
        Usage with DLX messages
        :return:
        """
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Origin": "*",
            'cache-control': "max-age=0",
            'content-type': "application/json",
        }
        req = requests.Session()
        cg_id = None
        response = req.request("GET", self.history_url, headers=headers)
        tree = html.fromstring(response.text)
        try:
            link_with_id = tree.xpath('//*[@id="statistics"]/div[2]/div[2]/div/div/div/a[1]/@data-url')
            cg_id = link_with_id[0].split('/')[2]
        except Exception:
            print('Invalid HTML or no Coingecko statistics.')

        rabbit_data = {
            'exchange_id': self.exchange.id,
            'source_url': self.future_url,
            'source_type': self.future_source,
            'source_id': cg_id
        }

        self.publishToRabbit(
            exchange='RABBIT_FUTURE_SOURCE_DLX_EXCHANGE',
            routing_key='RABBIT_FUTURE_SOURCE_MAIN_QUEUE',
            dlx=True,
            data=rabbit_data
        )

    def coinmarketcap_url(self):
        """
        Coinmarketcap future url
        Usage with DLX messages
        """
        rabbit_data = {
            'exchange_id': self.exchange.id,
            'source_url': self.future_url,
            'source_type': self.future_source
        }

        self.publishToRabbit(
            exchange='RABBIT_FUTURE_SOURCE_DLX_EXCHANGE',
            routing_key='RABBIT_FUTURE_SOURCE_MAIN_QUEUE',
            dlx=True,
            data=rabbit_data
        )

    def publishToRabbit(self, queue='', exchange='', routing_key='', data=None, dlx=False):
        """
        Publish message to rabbit mq
        :param queue:
        :param exchange:
        :param routing_key:
        :param data:
        :param dlx:
        """
        if data is None:
            data = {}
        producer = RabbitMQ(
            exchange=exchange,
            routing_key=routing_key,
            dlx=dlx,
            queue=queue
        )
        producer.publish(data)
