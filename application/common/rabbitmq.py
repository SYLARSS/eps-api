import pika
from pika.exceptions import ConnectionClosed
import os
import json
import sys


class RabbitMQ:
    """
    Rabbit MQ wrapper
    """
    def __init__(self, **kwargs):
        credentials = pika.PlainCredentials(os.environ.get('RABBIT_USER'), os.environ.get('RABBIT_PASS'))
        self.parameters = pika.ConnectionParameters(os.environ.get('RABBIT_HOST'), os.environ.get('RABBIT_PORT'), '/',
                                                    credentials)
        self.kwargs = kwargs
        self.connection = None
        self.routing_key = ''
        self.queue = ''
        self.exchange = ''
        self.hasdlx = False
        self.channel = None
        self.connect()

    def validate_params(self, key, value):
        """
        Validate Given Parameters
        :param key:
        :param value:
        """
        validation_list = ['exchange', 'queue', 'routing_key']
        if value and key in validation_list and value not in os.environ:
            print('Key ' + value + ' Is not in .env file')
            sys.exit()

    def create_dlx(self):
        self.channel.exchange_declare(
            exchange=os.environ.get('RABBIT_FUTURE_SOURCE_DLX_EXCHANGE'),
            exchange_type='direct',
            durable=True
        )

        self.channel.queue_declare(
            queue=os.environ.get('RABBIT_FUTURE_SOURCE_DLX_QUEUE'),
            arguments={
                'x-message-ttl': int(os.environ.get('RABBIT_MQ_DLX_TIME', 300000)),
                'x-dead-letter-exchange': os.environ.get('RABBIT_FUTURE_SOURCE_DLX_EXCHANGE'),
                'x-dead-letter-routing-key': os.environ.get('RABBIT_FUTURE_SOURCE_MAIN_QUEUE')
            },
            durable=True
        )
        self.channel.queue_declare(
            queue=os.environ.get('RABBIT_FUTURE_SOURCE_MAIN_QUEUE'),
            arguments={
                'x-dead-letter-exchange': os.environ.get('RABBIT_FUTURE_SOURCE_DLX_EXCHANGE'),
                'x-dead-letter-routing-key': os.environ.get('RABBIT_FUTURE_SOURCE_DLX_QUEUE')
            },
            durable=True
        )
        self.channel.queue_bind(
            queue=os.environ.get('RABBIT_FUTURE_SOURCE_MAIN_QUEUE'),
            exchange=os.environ.get('RABBIT_FUTURE_SOURCE_DLX_EXCHANGE')
        )

        self.channel.queue_bind(
            queue=os.environ.get('RABBIT_FUTURE_SOURCE_DLX_QUEUE'),
            exchange=os.environ.get('RABBIT_FUTURE_SOURCE_DLX_EXCHANGE')
        )

    def publish(self, data):
        """
        Publish Data with retry if no connection to rabbit
        :param data:
        """
        if not data:
            print('No data provided!')
            return
        try:
            self._publish(data)
        except ConnectionClosed:
            print('reconnecting to queue')
            self.connect()
            self._publish(data)

        self.connection.close()

    def consume(self, queue='', function='callback'):
        """
        Consume Messages from rabbit
        :param queue:
        :param function:
        """
        self.channel.basic_consume(on_message_callback=function, queue=queue)
        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

    def connect(self):
        """
        Create Rabbit Connection, Channel etc
        """
        self.connection = pika.BlockingConnection(self.parameters)
        self.channel = self.connection.channel()
        for key, value in self.kwargs.items():
            self.validate_params(key, value)
            if key == 'dlx' and value is True:
                self.hasdlx = True
                self.create_dlx()
            if key == 'queue' and value:
                self.queue = os.environ.get(value)
                self.channel.queue_declare(queue=self.queue, durable=True)
            if key == 'routing_key':
                self.routing_key = os.environ.get(value)
            if key == 'exchange' and value:
                self.exchange = os.environ.get(value)

    def _publish(self, data):
        """
        Publish Data to Rabbit Queue
        :param data:
        """
        self.channel.basic_publish(
            exchange=self.exchange,
            routing_key=self.routing_key,
            body=json.dumps(data)
        )
