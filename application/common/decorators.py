from flask_rest_jsonapi.exceptions import AccessDenied, ObjectNotFound
from helpers.utilities import safe_query, get_list_of_relation_ids
from sqlalchemy.orm.exc import NoResultFound
from flask import request
from app import db


def check_current_user_has_relation(param, relation, relation_param):
    """
    Check has user relation
    :param param: the key of related model
    :param relation: model
    :param relation_param:
    :return:
    """

    def decorator(func):
        def wrapper(self, view_kwargs):
            list_items = get_list_of_relation_ids(relation=relation, relation_param=relation_param)
            if view_kwargs.get(param) in list_items:
                return
            else:
                raise AccessDenied(
                    {'parameter': param},
                    "You don't have permissions.".format(view_kwargs[param])
                )

        return wrapper

    return decorator


def query_decorator_has_exchange(
    first_model,
    first_model_relation,
    relation_param,
    view_kwargs_param,
    second_model,
    filter_second_model_key
):
    """
    Check if current user has permissions to see relation
    :param first_model:
    :param first_model_relation:
    :param relation_param:
    :param view_kwargs_param:
    :param second_model:
    :param filter_second_model_key:
    :return:
    """

    def decorator(func):
        def wrapper(self, view_kwargs):
            search = request.args.get('search')
            list_items = get_list_of_relation_ids(relation=first_model_relation, relation_param=relation_param)

            query_ = self.session.query(first_model).filter(first_model.id.in_(list_items))

            if view_kwargs.get(view_kwargs_param):
                user = safe_query(
                    self,
                    second_model,
                    filter_second_model_key,
                    view_kwargs[view_kwargs_param],
                    view_kwargs_param
                )
                query = getattr(first_model, 'query')
                query_ = query.filter(getattr(first_model, 'users').any(id=getattr(user, filter_second_model_key)))
            if search:
                query_ = query_.filter(getattr(first_model, 'name').ilike(f'%{search}%'))
            return query_

        return wrapper

    return decorator


def query_decorator_simple(decorator_values):
    """
    Filter query by having relation model
    :param decorator_values:
    :return:
    """

    def decorator(func):
        def wrapper(self, view_kwargs):
            for key, values in decorator_values.items():
                query_ = self.session.query(values.get('primary_model'))
                if view_kwargs.get(key):
                    item = safe_query(
                        self, values.get('secondary_model'),
                        values.get('filter_secondary_model_key'),
                        view_kwargs[key],
                        key
                    )
                    query_ = getattr(values.get('primary_model'), 'query').filter(
                        getattr(values.get('primary_model'),
                                values.get('primary_model_relation')
                                ).any(id=getattr(item, values.get('filter_secondary_model_key')))
                    )
                return query_

        return wrapper

    return decorator


def query_list_volumes(decorator_values):
    """
    List volumes decorator to get list of user exchanges only
    with filter for date from and date to
    :param decorator_values:
    :return:
    """

    def decorator(func):
        def wrapper(self, view_kwargs):
            from_date = request.args.get('from')
            to_date = request.args.get('to')

            for key, values in decorator_values.items():
                list_items = get_list_of_relation_ids(relation=values.get('primary_model_relation'),
                                                      relation_param=values.get('filter_secondary_model_key'))
                query_ = self.session.query(values.get('primary_model'))
                if view_kwargs.get(key) is not None:
                    try:
                        self.session.query(values.get('secondary_model')).filter_by(id=view_kwargs[key]).one()
                    except NoResultFound:
                        raise ObjectNotFound(
                            {'parameter': key},
                            "{} : {} not found".format(values.get('secondary_model').__name__,
                                                       view_kwargs['exchange_id'])
                        )
                    if view_kwargs.get(key) not in list_items:
                        raise AccessDenied(
                            {'parameter': key},
                            "You don't have permissions.".format(view_kwargs[key])
                        )
                    query_ = query_.filter(getattr(values.get('primary_model'), key) == view_kwargs[key])
                    if from_date:
                        query_ = query_.filter(db.func.date(values.get('primary_model').date) >= from_date)
                    if to_date:
                        query_ = query_.filter(db.func.date(values.get('primary_model').date) <= to_date)
                return query_

        return wrapper

    return decorator
