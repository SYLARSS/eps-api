from helpers.text_helper import dasherize
from marshmallow_jsonapi.flask import Schema, Relationship
from marshmallow_jsonapi import fields
from marshmallow import validates_schema, ValidationError
from helpers.source_helper import getSource


class ExchangeSchema(Schema):
    """
    Exchange Schema
    """

    class Meta:
        """
        Meta class for Exchange Api Schema
        """
        type_ = 'exchange'
        self_view = 'v1.exchange_detail'
        self_view_kwargs = {'id': '<id>'}
        inflect = dasherize
        strict = True

    @validates_schema
    def validate_sources(self, data):
        future_source = data['future_source']
        history_source = data['history_source']
        f_source = getSource(future_source)
        h_source = getSource(history_source)
        if not f_source:
            raise ValidationError('Future Source is not valid.', 'future_source')

        if not h_source:
            raise ValidationError('History Source is not valid.', 'history_source')
        if h_source['source'] not in ['cryptocompare', 'coingecko']:
            raise ValidationError(
                'History source is invalid. Valid Sources are: [\'cryptocompare\',\'coingecko\']',
                'history_source')

    id = fields.Str(dump_only=True)
    name = fields.Str(required=True)
    website = fields.URL(required=True)
    future_source = fields.URL(required=True)
    history_source = fields.URL(required=True)
    created_at = fields.DateTime(dump_only=True)
    deleted_at = fields.DateTime(dump_only=True)

    users = Relationship(
        attribute='users',
        many=True,
        self_view='v1.exchange_users',
        self_view_kwargs={'id': '<id>'},
        related_view='v1.user_list',
        related_view_kwargs={'exchange_id': '<id>'},
        schema='UserSchema',
        type_='user')

    volumes = Relationship(
        attribute='volumes',
        many=True,
        self_view='v1.exchange_volumes',
        self_view_kwargs={'id': '<id>'},
        related_view='v1.exchange_volume_list',
        related_view_kwargs={'id': '<id>'},
        schema='ExchangeVolumeSchema',
        type_='exchange_volume')
