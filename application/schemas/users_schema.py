from helpers.text_helper import dasherize
from common.schema import use_defaults
from marshmallow_jsonapi.flask import Schema, Relationship
from marshmallow_jsonapi import fields


@use_defaults()
class UserSchema(Schema):
    """
    User Schema
    """
    class Meta:
        """
        Meta class for User Api Schema
        """
        type_ = 'user'
        self_view = 'v1.user_detail'
        self_view_kwargs = {'id': '<id>'}
        inflect = dasherize
        strict = True

    id = fields.Str(dump_only=True)
    email = fields.Str(required=True)
    full_name = fields.String(required=True)
    active = fields.Boolean(required=False)
    api_key = fields.Str(load_only=True)
    created_at = fields.DateTime(dump_only=True)
    deleted_at = fields.DateTime(dump_only=True)

    exchanges = Relationship(attribute='exchanges',
                             many=True,
                             self_view='v1.user_exchanges',
                             self_view_kwargs={'id': '<id>'},
                             related_view='v1.exchange_list',
                             related_view_kwargs={'user_id': '<id>'},
                             schema='ExchangeSchema',
                             type_='exchange')
