from helpers.text_helper import dasherize
from common.schema import use_defaults
from marshmallow_jsonapi.flask import Schema, Relationship
from marshmallow_jsonapi import fields


@use_defaults()
class ExchangeVolumeSchema(Schema):
    class Meta:
        """
        Meta class for Exchange Api Schema
        """
        type_ = 'exchange_volume'
        inflect = dasherize
        strict = True

    id = fields.Int(dump_only=True)
    date = fields.DateTime(required=True)
    volume_24_usd = fields.Integer()
    volume_24_btc = fields.Integer()
    volume_24_eth = fields.Integer()
    number_of_coins = fields.Integer()
    number_of_pairs = fields.Integer()
    exchange_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    deleted_at = fields.DateTime(dump_only=True)

    exchange = Relationship(
        attribute='exchange',
        self_view='v1.volume_exchange',
        self_view_kwargs={'id': '<id>'},
        related_view='v1.exchange_detail',
        related_view_kwargs={'volume_id': '<id>'},
        schema='ExchangeSourceSchema',
        type_='exchange_source')
