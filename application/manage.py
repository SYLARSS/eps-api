from flask_script import Manager
from app import pyBase, db
import datetime
from sqlalchemy import text
from helpers.utilities import datify

manager = Manager(pyBase)


def iterate_months(start_date, end_date):
    """Iterate monthly between two given dates.

    Emitted will be the first day of each month.

    >>> list(iterate_months(datetime.date(1999, 11, 1),
    ...                     datetime.date(2000, 2, 1)))
    [datetime.date(1999, 11, 1), datetime.date(1999, 12, 1),\
 datetime.date(2000, 1, 1), datetime.date(2000, 2, 1)]

    """
    assert isinstance(start_date, datetime.date)
    assert isinstance(end_date, datetime.date)
    assert start_date < end_date

    year = start_date.year
    month = start_date.month
    while True:
        current = datetime.date(year, month, 1)
        yield current
        if current.month == end_date.month and current.year == end_date.year:
            break
        else:
            month = ((month + 1) % 12) or 12
            if month == 1:
                year += 1


def format_month(date):
    return date.strftime("%Y-%m-%d")


@manager.command
def create_volume_partitions():
    """
    Create Exchange Volume Table Partitions
    :return:
    """
    print("Creating Volume Partitions")
    start = datify("2006-01-01")
    end = datify("2030-12-01")
    for entry in iterate_months(start, end):
        date = format_month(entry)
        split_date = date.split('-')
        table_name = 'exchange_volumes_y' + split_date[0] + 'm' + split_date[1]
        if split_date[1] == '12':
            year_to = int(split_date[0]) + 1
            month_to = 1
            date_to = format_month(datetime.datetime(year_to, month_to, 1))
        else:
            year_to = int(split_date[0])
            month_to = int(split_date[1]) + 1
            date_to = format_month(datetime.datetime(year_to, month_to, 1))

        sql = "CREATE TABLE IF NOT EXISTS " + table_name + " PARTITION OF exchange_volumes FOR VALUES FROM ('" + date + "') TO ('" + date_to + "');"
        db.engine.execute(text(sql))
    print("Done creating partitions")


if __name__ == "__main__":
    manager.run()
