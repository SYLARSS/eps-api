from tests.helpers.testing_helper import *


def test_exchange_create(client, admin, functional_dbsession):
    attributes = {
        "name": "Binance" + randomString2(5),
        "website": "https://binance.com",
        "future_source": "https://coinmarketcap.com/exchanges/binance/",
        "history_source": "https://coingecko.com/exchanges/binance/"
    }
    relationships = {
        'users': {
            'data': [
                {
                    'type': 'user',
                    'id': str(admin.id)
                }
            ]
        }
    }

    data = create_exchange(client, admin, attributes, 'exchange', '/v1/exchanges', relationships)

    # Check if creation response is correct
    assert 'data' in data
    assert 'attributes' in data['data']
    assert 'type' in data['data']
    assert 'id' in data['data']
    assert 'links' in data['data']
    assert 'website' in data['data']['attributes']
    assert 'name' in data['data']['attributes']
    assert 'future-source' in data['data']['attributes']
    assert 'history-source' in data['data']['attributes']

    assert data['data']['type'] == 'exchange'


def test_invalid_exchange_create_invalid_future_source(client, admin, functional_dbsession):
    attributes = {
        "name": "Binance" + randomString2(5),
        "website": "https://binance.com",
        "future_source": "https://zamunda.com/exchanges/binance/",
        "history_source": "https://coingecko.com/exchanges/binance/"
    }
    relationships = {
        'users': {
            'data': [
                {
                    'type': 'user',
                    'id': str(admin.id)
                }
            ]
        }
    }
    # Generate post request
    data = create_exchange(client, admin, attributes, 'exchange', '/v1/exchanges', relationships)

    response = client.post('/v1/exchanges', json=data, headers=create_valid_header(admin))
    assert response.status_code == 422


def test_invalid_exchange_create_invalid_history_source(client, admin, functional_dbsession):
    attributes = {
        "name": "Binance" + randomString2(5),
        "website": "https://binance.com",
        "future_source": "https://coinmarketcap.com/exchanges/binance/",
        "history_source": "https://zamunda.com/exchanges/binance/"
    }
    relationships = {
        'users': {
            'data': [
                {
                    'type': 'user',
                    'id': str(admin.id)
                }
            ]
        }
    }

    # Generate post request
    data = create_exchange(client, admin, attributes, 'exchange', '/v1/exchanges', relationships)

    response = client.post('/v1/exchanges', json=data, headers=create_valid_header(admin))
    assert response.status_code == 422


def test_exchanges_list(client, admin, functional_dbsession):
    response = client.get('/v1/exchanges', headers=create_valid_header(admin))
    data = json.loads(response.data)
    assert len(data['data']) > 0
    assert type(data['data']) == list
    asset = data['data'][0]
    assert 'type' in asset
    assert 'attributes' in asset
    assert 'id' in asset
    assert 'relationships' in asset
    asset_attr = asset['attributes']
    assert 'name' in asset_attr
    assert 'website' in asset_attr
    assert 'history-source' in asset_attr
    assert 'future-source' in asset_attr


def test_exchange_detail(client, admin, functional_dbsession):
    attributes = {
        "name": "kucoin22" + randomString2(5),
        "website": "https://kucoin22.com",
        "future_source": "https://coinmarketcap.com/exchanges/kucoin/",
        "history_source": "https://coingecko.com/exchanges/kucoin/"
    }
    relationships = {
        'users': {
            'data': [
                {
                    'type': 'user',
                    'id': str(admin.id)
                }
            ]
        }
    }
    # Generate post request
    create_data = create_exchange(client, admin, attributes, 'exchange', '/v1/exchanges', relationships)
    exchange_id = create_data['data']['id']

    response = client.get('/v1/exchanges/' + str(exchange_id), headers=create_valid_header(admin))
    data = json.loads(response.data)
    assert 'data' in data
    assert 'attributes' in data['data']
    assert 'type' in data['data']
    assert 'id' in data['data']
    assert 'links' in data['data']
    assert 'website' in data['data']['attributes']
    assert 'name' in data['data']['attributes']
    assert 'future-source' in data['data']['attributes']
    assert 'history-source' in data['data']['attributes']


def test_invalid_exchange_detail(client, admin, functional_dbsession):
    exchange_id = "0"
    response = client.get('/v1/exchanges/' + exchange_id, headers=create_valid_header(admin))
    assert response.status_code == 403
