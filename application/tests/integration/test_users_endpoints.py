import json
from app.factories.authorization import UserFactory
import random
import string
from app.models.user_model import MAX_EMAIL_LENGTH, MAX_FULL_NAME_LENGTH
from tests.helpers.testing_helper import data_generator


def create_valid_header(admin):
    return {
        "Authorization": "API_KEY {}".format(admin.api_key)
    }


def test_user_create(client, admin, functional_dbsession):
    #  Create main user
    new_user = UserFactory.build()
    attributes = {
        "full_name": new_user.full_name,
        "email": new_user.email
    }
    # Generate post request
    data = data_generator(attributes, 'user')

    response = client.post('/v1/users', json=data, headers=create_valid_header(admin))
    data = json.loads(response.data)

    # Check if creation response is correct
    assert 'data' in data
    assert 'attributes' in data['data']
    assert 'type' in data['data']
    assert 'id' in data['data']
    assert 'links' in data['data']
    assert 'email' in data['data']['attributes']
    assert 'full-name' in data['data']['attributes']

    assert data['data']['type'] == 'user'
    assert data['data']['attributes']['email'] == new_user.email
    assert data['data']['attributes']['full-name'] == new_user.full_name

    # Check if the detailed page works correctly
    link = data['data']['links']['self']

    response = client.get(link, headers=create_valid_header(admin))
    data = json.loads(response.data)

    assert data['data']['type'] == 'user'
    assert data['data']['attributes']['email'] == new_user.email
    assert data['data']['attributes']['full-name'] == new_user.full_name


def test_user_invalid(client, admin, functional_dbsession):
    response = client.delete('/v1/users/{}'.format(0), headers=create_valid_header(admin))
    assert response.status_code == 404

    decoded_data = json.loads(response.data)

    assert 'errors' in decoded_data
    initial_error = decoded_data['errors'][0]

    assert 'status' in initial_error
    assert "404" == initial_error['status']


def test_invalid_user_create(client, admin, functional_dbsession):
    fullname = ''.join(random.choice(string.ascii_lowercase) for x in range(MAX_FULL_NAME_LENGTH + 1))
    email = ''.join(random.choice(string.ascii_lowercase) for x in range(MAX_EMAIL_LENGTH + 1))
    # Generate post request

    attributes = {
        "full_name": fullname,
        "email": email
    }

    data = data_generator(attributes, 'user')

    response = client.post('/v1/users', json=data, headers=create_valid_header(admin))
    assert response.status_code == 422


def test_no_api_key(client):
    #  Create main user
    new_user = UserFactory.build()
    # Generate post request
    attributes = {
        "full_name": new_user.full_name,
        "email": new_user.email
    }
    data = data_generator(attributes, 'user')

    response = client.post('/v1/users', json=data, headers={})
    data = json.loads(response.data)
    assert response.status_code == 400
    assert data['errors'][0]['title'] == "Bad Request"


def test_invalid_type(client, admin, functional_dbsession):
    #  Create main user
    new_user = UserFactory.build()
    # Generate post request
    attributes = {
        "full_name": new_user.full_name,
        "email": new_user.email
    }
    data = data_generator(attributes, 'exchange')

    response = client.post('/v1/users', json=data, headers=create_valid_header(admin))
    data = json.loads(response.data)
    assert response.status_code == 409
    assert data['errors'][0]['title'] == "Incorrect type"


def test_users_list(client, admin, functional_dbsession):
    response = client.get('/v1/users', headers=create_valid_header(admin))
    data = json.loads(response.data)
    assert len(data['data']) > 0


def test_user_detail(client, admin):
    user_id = str(admin.id)
    response = client.get('/v1/users/' + user_id, headers=create_valid_header(admin))
    data = json.loads(response.data)
    assert len(data['data']) > 0


def test_invalid_user_detail(client, admin, functional_dbsession):
    user_id = "88888"
    response = client.get('/v1/users/' + str(user_id), headers=create_valid_header(admin))
    data = json.loads(response.data)
    assert response.status_code == 200
    assert not data['data']
