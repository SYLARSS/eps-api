from helpers.source_helper import getSource


def test_get_source_positive():
    sources = {
        'cryptocompare': {
            'url': 'https://www.cryptocompare.com/exchanges/coinbase/overview',
            'slug': 'coinbase'
        },
        'coinmarketcap': {
            'url': 'https://coinmarketcap.com/exchanges/binance/',
            'slug': 'binance'
        },
        'coinpaprika': {
            'url': 'https://coinpaprika.com/exchanges/binance/',
            'slug': 'binance'
        },
        'coingecko': {
            'url': 'https://www.coingecko.com/exchanges/binance/',
            'slug': 'binance'
        }
    }
    for source, attr in sources.items():
        validateSource = getSource(attr['url'])
        assert validateSource['source'] == source
        assert validateSource['slug'] == attr['slug']


def test_get_source_negative():
    source = getSource("https://www.a.com/exchanges/coinbase/overview")
    assert source is False


def test_text_helper():
    from helpers.text_helper import dasherize, generate_key

    string = 'string_to_be_dasharized'
    hash_generated = generate_key(10)

    assert dasherize(string) == 'string-to-be-dasharized'
    assert len(hash_generated) == 10
