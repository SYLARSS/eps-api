import pytest
import os
from app import create_app
from app import db
from app.factories.authorization import UserFactory
from sqlalchemy_utils import create_database, database_exists
from config import TestConfig


@pytest.fixture(scope='session')
def application():
    os.environ['FLASK_CONFIG'] = 'testing'

    flask, sqlalchemy = create_app()
    db.init_app(flask)
    db.app = flask
    if not database_exists(TestConfig.SQLALCHEMY_DATABASE_URI):
        create_database(TestConfig.SQLALCHEMY_DATABASE_URI)
    db.create_all()

    yield flask

    # db.drop_all()


@pytest.fixture(scope='session')
def client(application):
    client = application.test_client()
    yield client


@pytest.fixture(scope='session')
def session(application):
    db.app = application
    admin = UserFactory.build()

    db.session.add(admin)
    db.session.expire_on_commit = False
    db.session.commit()
    return db.session()


@pytest.fixture(scope='session')
def functional_dbsession(session):
    session.expire_on_commit = False
    session.commit()
