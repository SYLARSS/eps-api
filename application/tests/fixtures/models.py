import pytest
from app.models.user_model import UserModel


@pytest.fixture(scope='session')
def admin(session):
    admin = session.query(UserModel).first()
    return admin
