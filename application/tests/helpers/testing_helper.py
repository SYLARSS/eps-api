import json
import random
import string


def data_generator(attributes, type_arg, relationships=False):
    data = {
        "data": {
            "type": type_arg,
            "attributes": attributes
        }
    }

    if relationships:
        data['data']['relationships'] = relationships

    return data


def create_exchange(client, admin, attributes, type_arg, endpoint, relationships=None):
    data = data_generator(attributes, type_arg, relationships)
    response = client.post(endpoint, json=data, headers=create_valid_header(admin))
    return json.loads(response.data)


def create_valid_header(admin):
    return {
        "Authorization": "API_KEY {}".format(admin.api_key)
    }


def randomString2(stringLength=8):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.sample(letters, stringLength))
