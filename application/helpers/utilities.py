from sqlalchemy.orm.exc import NoResultFound
from flask_rest_jsonapi.exceptions import ObjectNotFound
import datetime
from helpers.auth_helper import current_user


def safe_query(self, model, column_name, value, parameter_name):
    """
    Wrapper query to properly raise exception
    :param self:
    :param model: db Model to be queried
    :param column_name: name of the column to be queried for the given value
    :param value: value to be queried against the given column name, e.g view_kwargs['event_id']
    :param parameter_name: Name of parameter to be printed in json-api error message eg 'event_id'
    :return:
    """
    try:
        record = self.session.query(model).filter(getattr(model, column_name) == value).one()
    except NoResultFound:
        raise ObjectNotFound({'parameter': '{}'.format(parameter_name)},
                             "{}: {} not found".format(model.__name__, value))
    else:
        return record


def datify(date):
    """
    Create date from string
    :param date:
    :return:
    """
    if isinstance(date, datetime.date):
        return date
    elif isinstance(date, datetime.datetime):
        return date.date()
    elif date is None:
        raise Exception('Date must be string, date or datetime')
    else:
        return datetime.datetime.strptime(date, "%Y-%m-%d")


def get_list_of_relation_ids(relation, relation_param):
    list_exchanges = []
    list_items = getattr(current_user(), relation)
    for item in list_items:
        list_exchanges.append(getattr(item, relation_param))
    return list_exchanges
