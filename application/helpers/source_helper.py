import re

regex = {
    'coinmarketcap': {
        'regex': r'^https?:\/\/(www\.)?coinmarketcap.com\/exchanges\/([a-z0-9-_.]+)(\/)?',
        'match_key': 2,
        'source': 'coinmarketcap'
    },
    'coinpaprika': {
        'regex': r'^https?:\/\/(www\.)?coinpaprika\.com\/exchanges\/([a-z0-9]+)(\/)?',
        'match_key': 2,
        'source': 'coinpaprika'
    },
    'coingecko': {
        'regex': r'^(https?:\/\/)?(www\.)?coingecko\.com\/(en\/)?exchanges\/([a-zA-Z0-9]+)(\/)?',
        'match_key': 4,
        'source': 'coingecko'
    },
    'cryptocompare': {
        'regex': r'^(https?:\/\/)?(www\.)?cryptocompare\.com\/exchanges\/(.+)\/',
        'match_key': 3,
        'source': 'cryptocompare'
    },
}


def getSource(url):
    """
    Get source and slug by url
    :param url:
    :return:
    """
    for a in regex.values():
        regex_match = re.match(a['regex'], url)
        if bool(regex_match) and regex_match[a['match_key']]:
            return {'slug': regex_match[a['match_key']], 'source': a['source']}
    return False
