import os


def get_env(key):
    """
    Get environment variable from .env
    :param key:
    :return:
    """
    return os.environ.get(key)


def create_db_string(database):
    """
    Create DB Connection String
    :param database:
    :return:
    """
    if not get_env('DB_CONNECTION'):
        raise Exception('DB_CONNECTION is not defined in .env')
    if not get_env('DB_USER'):
        raise Exception('DB_USER is not defined in .env')

    if not get_env('DB_PASS'):
        raise Exception('DB_PASS is not defined in .env')

    if not get_env('DB_HOST'):
        raise Exception('DB_HOST is not defined in .env')

    if not get_env('DB_PORT'):
        raise Exception('DB_PORT is not defined in .env')
    if not get_env(database):
        raise Exception(database + ' is not defined in .env')

    return get_env('DB_CONNECTION') + \
           '://' + get_env('DB_USER') + \
           ':' + get_env('DB_PASS') + \
           '@' + get_env('DB_HOST') + \
           ':' + get_env('DB_PORT') + \
           '/' + get_env(database)
