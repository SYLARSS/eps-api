from app.models.user_model import UserModel
from functools import wraps
from flask import request
from helpers.errors_helper import UnauthorizedError, BadRequestError
from helpers.exceptions_helper import ServerError
import re


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        # validate the API Key
        if 'Authorization' in request.headers:
            text = request.headers.get('Authorization')
            pattern = '^API_KEY\s(?P<api_key>\w{32})$'
            regex = re.compile(pattern)
            if re.search(regex, text) is None:
                return BadRequestError({'header': 'Authorization'}, 'Wrong format!').respond()
        else:
            return BadRequestError({'header': 'Authorization'}, 'Missing header!').respond()

        # check if the logged user has been existing by API provided key
        if current_user() is None:
            return UnauthorizedError(
                {},
                'The Endpoint you try to use is secured. Please use your API_KEY Header'
            ).respond()
        return f(*args, **kwargs)

    return decorated


def get_authorization_api_key():
    """
    :return:
    """
    try:
        if 'Authorization' in request.headers:
            text = request.headers.get('Authorization')
            pattern = '^API_KEY\s(?P<api_key>\w{32})$'
            regex = re.compile(pattern)
            if re.search(regex, text):
                return re.match(regex, text).group('api_key')
        return False
    except Exception as ex:
        raise ServerError(ex, source={"method": __name__}, title='Server Error')


def current_user():
    """
    Get the logged user. If there no logged user, we are going to  return None
    :return:
    """
    return UserModel.query.filter_by(api_key=get_authorization_api_key()).first()


def permission_manager(view, view_args, view_kwargs, *args, **kwargs):
    print(kwargs)
    pass


def has_access(access_level, **kwargs):
    """
    The method to check if the logged in user has specified access
    level or nor
    :param string access_level: name of access level
    :param dict kwargs: This is directly passed to permission manager
    :return: bool: True if passes the access else False
    """
    print(kwargs)
