# PLEASE PUT ALL FUNCTIONS WHICH PERFORM GENERAL FORMATTING ON ANY DATATYPE WITHOUT USING ANY
# MODULES RELATED TO THE EVENT-SYSTEM i.e FUNCTIONS SPECIFIC TO DB MODELS E.G A FUNCTION JUST FOR ROLE_INVITES
import random
import string


def dasherize(text):
    """
    Replace _ with -
    :param text:
    :return:
    """
    return text.replace('_', '-')


def generate_key(length=0):
    """
    Returns random generated api key by length provided
    :param length:
    :return:
    """
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


def create_assert_message(key, error):
    """
    Pytest validation error message
    :param key:
    :param error:
    :return:
    """
    return {
        'title': '"Couldn\'t validate {}".'.format(key),
        'error': error
    }
