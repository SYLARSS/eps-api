from flask_rest_jsonapi.exceptions import JsonApiException


class ServerError(JsonApiException):
    """ServerError error"""

    title = 'Server Error'
    status = '500'


class UnprocessableEntity(JsonApiException):
    """
    Default class for 422 Error
    """
    title = "Unprocessable Entity"
    status = 422
