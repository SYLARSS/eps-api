from helpers.config_helper import create_db_string


class Config(object):
    """
    Common configurations
    """
    SQLALCHEMY_DATABASE_URI = create_db_string('DB_DATABASE')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPOGATE_ERROR = True
    PAGE_SIZE = 25


class TestConfig(Config):
    """
    Common configurations
    """
    SQLALCHEMY_DATABASE_URI = create_db_string('DB_DATABASE_TEST')
    PYTHONDONTWRITEBYTECODE = 1


class DevelopmentConfig(Config):
    """
    Development configurations
    """

    DEBUG = True
    SQLALCHEMY_ECHO = False


class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestConfig
}
